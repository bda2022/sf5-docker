#!/bin/sh

# Aliases
alias sf="php bin/console"
alias sft="php bin/phpunit $1 --testdox"
alias sfc="sf cache:clear"

# Locale
export LANG="fr_FR.utf8"
